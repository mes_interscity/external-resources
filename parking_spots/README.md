# Running Parking Spot script

This project simulates parking spots placed at USP and Municipal Theater of
São Paulo city.

## Setup

* Install Ruby 2.4.0
* Install the required gems: `bundle install`

Run the following command if you want to use only a subset of parking spots:
* cp parking_spots.yml{.half,}
Run the following command if you want to use all parking spots:
* cp parking_spots.yml{.full,}

## Start collector

To run the script to collect data from the simulated parking spots
you need to set a environment variable with the host of Resource Adaptor.
Thus, you may run the following:
```
ADAPTOR_HOST=localhost:3002 ruby main.rb
```

## TODO

* Whenever you run the scripts, it register all parking spots as new resources.
To avoid it, we need to check in the platform for existing resources with 
the same latitude and longitude information.
